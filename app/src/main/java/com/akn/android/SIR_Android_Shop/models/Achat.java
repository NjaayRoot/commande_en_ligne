package com.akn.android.SIR_Android_Shop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macbook on 08/10/2017.
 */

public class Achat {
    private Met met;
    private int idAchat;
    @SerializedName("quantite")
    private int quantite;
    @SerializedName("totalAchat")
    private int totalAchat;
    @SerializedName("condiment")
    private String condiment;
    @SerializedName("consommation")
    private String consommation;


    public Achat(int idAchat,int quantite,int totalAchat,String condiment,String consommation){
        this.idAchat= idAchat;
        this.quantite= quantite;
        this.totalAchat= totalAchat;
        this.condiment= condiment;
        this.consommation= consommation;
    }

    public Achat(Met met){
        this.met= met;

    }

    public int getIdAchat() {
        return idAchat;
    }

    public void setIdAchat(int idAchat) {
        this.idAchat = idAchat;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Met getMet() {
        return met;
    }

    public void setMet(Met met) {
        this.met = met;
    }

    public int getTotalAchat() {
        return met.getQuantite() * Integer.parseInt(met.getPrixUnitaire());

    }

    public void setTotalAchat(int totalAchat) {
        this.totalAchat = totalAchat;
    }

    public String getCondiment() {
        return condiment;
    }

    public void setCondiment(String condiment) {
        this.condiment = condiment;
    }

    public String getConsommation() {
        return consommation;
    }

    public void setConsommation(String consommation) {
        this.consommation = consommation;
    }
}
