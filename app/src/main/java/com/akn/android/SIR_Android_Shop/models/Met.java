package com.akn.android.SIR_Android_Shop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AKN on 25/07/2017.
 */

public class Met {
    //private int idMet;
    @SerializedName("nom")
    private String nomMet;
    @SerializedName("tpsCuisson")
    private String tps_cuisson;
    @SerializedName("prixU")
    private String prixUnitaire;
    @SerializedName("image")
    private String pic;
    @SerializedName("idMet")
    private String idMet;

    private String cons;
    private String condiment;
    private int quantite;

    public Met(String idMet,String nomMet, String prixUnitaire, String pic,String tps_cuisson) {
        this.idMet = idMet;
        this.pic = pic;
        this.prixUnitaire = prixUnitaire;
        this.nomMet = nomMet;
        this.tps_cuisson = tps_cuisson;

    }

    public String getIdMet() {
        return idMet;
    }

    public void setIdMet(String idMet) {
        this.idMet = idMet;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(String prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public String getNomMet() {
        return nomMet;
    }

    public void setNomMet(String nomMet) {
        this.nomMet = nomMet;
    }

    public String getTps_cuisson() {
        return tps_cuisson;
    }

    public void setTps_cuisson(String tps_cuisson) {
        this.tps_cuisson = tps_cuisson;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getCons() {
        return cons;
    }

    public void setCons(String cons) {
        this.cons = cons;
    }

    public String getCondiment() {
        return condiment;
    }

    public void setCondiment(String condiment) {
        this.condiment = condiment;
    }
}
