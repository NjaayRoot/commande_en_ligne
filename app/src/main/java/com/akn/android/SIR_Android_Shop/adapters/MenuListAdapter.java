package com.akn.android.SIR_Android_Shop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.activity.MenuActivity;
import com.akn.android.SIR_Android_Shop.models.Menu;

import java.util.List;



public class MenuListAdapter extends BaseAdapter {

	private List<Menu> listMenu = null;
	private LayoutInflater layoutInflater;
	private Context context;
	private int lastPosition = -1;

 	@SuppressWarnings("unchecked")
	public MenuListAdapter(Context context, Object o) {
		this.listMenu = (List<Menu>)o;
		layoutInflater = LayoutInflater.from(context);
 		this.context = context;
	}

	@Override
	public int getCount() {
 		return listMenu.size();
	}

	@Override
	public Menu getItem(int position) {
 		return listMenu.get(position);
	}

	@Override
	public long getItemId(int arg0) {
 		return arg0;
	}

	static class ViewHolder {
		TextView titre;
 		ImageView image;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;

		if (convertView == null) {
			
			convertView = layoutInflater.inflate(R.layout.menu_row, null);
			holder = new ViewHolder();
			// initializ the views
  			holder.image = (ImageView) convertView.findViewById(R.id.image);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
        System.out.println(MenuActivity.listMenu.get(position).getImage());

		//int loader = R.drawable.ba;
		//String image_url = MenuActivity.listMenu.get(position).getImage();
        //ImageLoader imgLoader = new ImageLoader(context);
        //imgLoader.DisplayImage(image_url, loader, holder.image);

        int loader =  R.drawable.bt_entree;
		if(MenuActivity.listMenu.get(position).getImage().equals("entree"))
		{
			loader = R.drawable.bt_entree;
		}
		else if(MenuActivity.listMenu.get(position).getImage().equals("plat"))
		{
			loader = R.drawable.bt_plat;
		}
		else if(MenuActivity.listMenu.get(position).getImage().equals("dessert"))
		{
			loader = R.drawable.bt_dessert;
		}
		else if(MenuActivity.listMenu.get(position).getImage().equals("boisson"))
		{
			loader = R.drawable.bt_boisson;
		}
		else if(MenuActivity.listMenu.get(position).getImage().equals("sandwiche"))
		{
			loader = R.drawable.bt_swnch;
		}
		else if(MenuActivity.listMenu.get(position).getImage().equals("petitdej"))
		{
			loader = R.drawable.bt_ptitdej;
		}
		holder.image.setImageDrawable(context.getResources().getDrawable(loader));


		lastPosition = position;
		return convertView;

	}
}
