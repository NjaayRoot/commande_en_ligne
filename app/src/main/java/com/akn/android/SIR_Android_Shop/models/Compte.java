package com.akn.android.SIR_Android_Shop.models;

/**
 * Created by macbook on 10/10/2017.
 */

public class Compte {
    private int idUser;
    private String nomUser,pnomUser,login,pass,pp;

    public Compte(int idUser, String nomUser, String pnomUser, String login, String pass, String pp) {
        this.idUser = idUser;
        this.nomUser = nomUser;
        this.pnomUser = pnomUser;
        this.login = login;
        this.pass = pass;
        this.pp = pp;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNomUser() {
        return nomUser;
    }

    public void setNomUser(String nomUser) {
        this.nomUser = nomUser;
    }

    public String getPnomUser() {
        return pnomUser;
    }

    public void setPnomUser(String pnomUser) {
        this.pnomUser = pnomUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPp() {
        return pp;
    }

    public void setPp(String pp) {
        this.pp = pp;
    }
}
