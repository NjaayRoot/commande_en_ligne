package com.akn.android.SIR_Android_Shop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Commande implements Serializable {

    private Panier panier;

    @SerializedName("date_cmd")
    private String dateCmd;

    public Commande() {
        
    }

   

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public String getDateCmd() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE d MMMM yyyy", Locale.FRENCH);
        String today = sdf.format(new Date());
         sdf.applyPattern("HH:mm:ss");
        today+=" à "+ sdf.format(new Date());
        return today;
    }

    public void setDateCmd(String dateCmd) {
        this.dateCmd = dateCmd;
    }

}
