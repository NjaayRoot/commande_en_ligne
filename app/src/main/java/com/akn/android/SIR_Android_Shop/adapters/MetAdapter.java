package com.akn.android.SIR_Android_Shop.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.models.Met;

import java.util.List;

/**
 * Created by AKN on 22/07/2017.
 */

public class MetAdapter extends RecyclerView.Adapter<MetAdapter.ViewHolder> {

    private List<Met> metList;


    //Provide a reference to the views for each data item
    //Complex data items may need more than one view per item, and
    //you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder{
        //each data item is just a string in this case
        public TextView tvIdMet,tvNomMet,tvPrixUnitaire,tvTps_cuisson;
        public final ImageView ivPic;

        public ViewHolder(View v) {
            super(v);
            tvIdMet = (TextView)v.findViewById(R.id.tv_id_met);
            tvNomMet = (TextView) v.findViewById(R.id.tv_nom_met);
            tvPrixUnitaire = (TextView) v.findViewById(R.id.tv_prix_unitaire);
            tvTps_cuisson = (TextView) v.findViewById(R.id.tv_tps_cuisson);
            ivPic = (ImageView) v.findViewById(R.id.iv_pic);
        }
    }

    //Provide a suitable constructor
    public MetAdapter(List<Met> metList){

        this.metList = metList;

    }

    //Create new views (invoked by the layout manager)
    @Override
    public MetAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Creating a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,parent,false);

        //set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    //Replace the contents of a view (invoked by the layout manager
    @Override
    public void onBindViewHolder(final MetAdapter.ViewHolder holder, int position) {

        // - get element from arraylist at this position
        // - replace the contents of the view with that element

        final Met met = metList.get(position);
        holder.tvIdMet.setText(String.valueOf(met.getIdMet()));
        holder.tvNomMet.setText(met.getNomMet());
        holder.tvPrixUnitaire.setText(met.getPrixUnitaire());
        holder.tvTps_cuisson.setText(met.getTps_cuisson());



      // holder.ivPic.setImageResource(met.getPic());

       /* final String image= met.getPic();
        final ImageView picture =holder.ivPic;
        Picasso.with(context)
                .load("http://192.168.21.2/android/image/"+image)
                .into(picture); */
/*
         Glide.with(context)
                .load("http://192.168.21.2/android/mets.php")
                .centerCrop()
                .placeholder(R.drawable.f1)
                .into(picture);
*/


       /*new Thread(new Runnable() {
           @Override
           public void run() {
               try {

                   InputStream inputStream= new URL("http://192.168.21.2/android/image"+image).openStream();
                   Bitmap bitmap= BitmapFactory.decodeStream(inputStream);
                   picture.setImageBitmap(bitmap);

               }catch (Exception e) {
                   e.printStackTrace();
               }
           }
       }).start(); */
    }

    @Override
    public int getItemCount() {
        return metList.size();
    }
}

