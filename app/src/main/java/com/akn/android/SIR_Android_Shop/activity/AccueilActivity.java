package com.akn.android.SIR_Android_Shop.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.PanierListAdapter;
import com.akn.android.SIR_Android_Shop.models.Accueil;
import com.androidhive.imagefromurl.Utils;

import java.util.ArrayList;
import java.util.List;

public class AccueilActivity extends AppCompatActivity {

   private ListView listView;
    public static  List<Accueil> listAccueil=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_accueil);
        Toolbar   toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listRubrique);

        listAccueil=new ArrayList<>();
        Accueil accueil=new Accueil();
        accueil.setName("Menu");
        accueil.setImage(Utils.URL_IMAGE_SERVER+"c1.jpg");
        listAccueil.add(accueil);

        accueil=new Accueil();
        accueil.setName("Mets");
        accueil.setImage(Utils.URL_IMAGE_SERVER+"c2.jpg");
        listAccueil.add(accueil);

        accueil=new Accueil();
        accueil.setName("Panier");
        accueil.setImage(Utils.URL_IMAGE_SERVER+"c3.jpg");
        listAccueil.add(accueil);

        listView.setAdapter(new PanierListAdapter(getApplicationContext(), listAccueil));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(i==0)
                {
                    startActivity(new Intent(AccueilActivity.this, MenuActivity.class));
                }
                else   if(i==1)
                {

                }
                if(i==2)
                {

                }
            }
        });

    }
}
