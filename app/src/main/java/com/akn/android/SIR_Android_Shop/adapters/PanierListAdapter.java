package com.akn.android.SIR_Android_Shop.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.activity.PanierActivity;
import com.akn.android.SIR_Android_Shop.models.Achat;
import com.akn.android.SIR_Android_Shop.models.Met;

import com.akn.android.SIR_Android_Shop.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.met;
import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;
import static java.lang.Integer.parseInt;

public class PanierListAdapter extends BaseAdapter {

	private List<Achat> listeAchats= null;
	//public static  List<Met> metList = null;
	private LayoutInflater layoutInflater;
	private Context context;
	private int lastPosition = -1;
	public static  long sommeTotal= 0;
    public static Met positions;




	@SuppressWarnings("unchecked")
	public PanierListAdapter(Context context, Object o) {
		this.listeAchats = (List<Achat>)o;
		layoutInflater = LayoutInflater.from(context);
 		this.context = context;
	}



	@Override
	public int getCount() {
 		return paniers.getListeAchats().size();
	}

	@Override
	public Achat getItem(int position) {
 		return paniers.getListeAchats().get(position);
	}

	@Override
	public long getItemId(int arg0) {
 		return arg0;
	}

	static class ViewHolder {

        public TextView tvIdMet, tvNomMet, tvPrixUnitaire, tvQuantite, tvCons, tvCondiment, edit_qantite, tpsCuisson;
		public Button btn_minus,btn_plus;
		public  ImageView ivPic, Img_delete ;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final ViewHolder holder;

		if (convertView == null) {
			
			convertView = layoutInflater.inflate(R.layout.item_list, null);
			holder = new ViewHolder();
			// initializ the views
			holder.tvNomMet = (TextView) convertView.findViewById(R.id.list_title);
			holder.tvPrixUnitaire = (TextView) convertView.findViewById(R.id.list_prix);
			holder.tvQuantite = (TextView) convertView.findViewById(R.id.list_quantite);
			holder.tvCons = (TextView) convertView.findViewById(R.id.list_cons);
			holder.tvCondiment = (TextView) convertView.findViewById(R.id.list_condiments);
			holder.ivPic = (ImageView) convertView.findViewById(R.id.list_avatar);
			holder.edit_qantite = (TextView) convertView.findViewById(R.id.edit_text);
			holder.btn_minus = (Button) convertView.findViewById(R.id.btn_minus);
			holder.btn_plus = (Button) convertView.findViewById(R.id.btn_plus);
			holder.Img_delete = (ImageView)convertView.findViewById(R.id.Img_delete);
			//holder.tpsCuisson = (TextView)convertView.findViewById(R.id.tv_tps_cuisson);
			//holder.valeurTotal = (TextView) convertView.findViewById(R.id.valeurTotal);

			PanierActivity.txtValeurTotal.setText("00 FCFA");
			//sommeTotal=0;

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		//Set all the views
		holder.tvNomMet.setText(paniers.getListeAchats().get(position).getMet().getNomMet().toUpperCase()+"");
		holder.tvPrixUnitaire.setText("Prix : "+paniers.getListeAchats().get(position).getTotalAchat()+" FCFA"+"");
		holder.tvQuantite.setText("Quantite : "+paniers.getListeAchats().get(position).getMet().getQuantite()+"");
		holder.tvCons.setText("Consommation : "+paniers.getListeAchats().get(position).getMet().getCons()+"");

		String accompagnements="";
		if (met.equalsIgnoreCase("dessert")){
		    accompagnements="Parfum : ";
			holder.tvCondiment.setText(accompagnements+paniers.getListeAchats().get(position).getMet().getCondiment()+"");
		}
		else {
            accompagnements="Accompagnements : ";
            holder.tvCondiment.setText(accompagnements+paniers.getListeAchats().get(position).getMet().getCondiment()+"");
        }

        holder.edit_qantite.setText(paniers.getListeAchats().get(position).getMet().getQuantite()+"");
//		holder.ivPic.setImageResource(Integer.parseInt(metList.get(position).getPic()));

		//int loader = 0;
		//System.out.println(image_url);
		//ImageLoader imgLoader = new ImageLoader(context).DisplayImage(Utils.URL_IMAGE_SERVER+image_url, loader, holder.ivPic);

		String image_url = paniers.getListeAchats().get(position).getMet().getPic();

		//String picUrl=BASE_URL+image_url;

		Glide.with(context).load(image_url)
				.thumbnail(0.0f)
				.crossFade()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.into(holder.ivPic);


		positions = paniers.getListeAchats().get(position).getMet();

        //Clic on image delete
		holder.Img_delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                //call the method supprimerAchat in Panier model
                paniers.supprimerAchat(paniers.getListeAchats().get(position).getMet());

                PanierActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");
				notifyDataSetChanged();

			}
		});

        //set total view
		PanierActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");

        // if we wanna minus one met in the cart
		holder.btn_minus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				int qte= paniers.getListeAchats().get(position).getMet().getQuantite();
                if (qte == 1){
                    //call the method supprimerAchat in Panier model
                    paniers.supprimerAchat(paniers.getListeAchats().get(position).getMet());

                    PanierActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");
                    notifyDataSetChanged();
                }

                else {
                    //call the method supprimerAchat in Panier model
                    paniers.supprimerAchat(paniers.getListeAchats().get(position).getMet());

                    PanierActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");
                    holder.tvPrixUnitaire.setText("Prix : "+paniers.getListeAchats().get(position).getTotalAchat()+" FCFA");
                    holder.tvQuantite.setText("Quantite : "+paniers.getListeAchats().get(position).getMet().getQuantite()+"");
                    holder.edit_qantite.setText(paniers.getListeAchats().get(position).getMet().getQuantite()+"");

                }


			}
		});

        //if we wanna plus one met in the cart
		holder.btn_plus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

                //call the method ajouterAchat in Panier model
                paniers.ajouterAchat(paniers.getListeAchats().get(position).getMet());
				PanierActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");
				holder.tvPrixUnitaire.setText("Prix : "+paniers.getListeAchats().get(position).getTotalAchat()+" FCFA");
				holder.tvQuantite.setText("Quantite : "+paniers.getListeAchats().get(position).getMet().getQuantite()+"");
				holder.edit_qantite.setText(paniers.getListeAchats().get(position).getMet().getQuantite()+"");

			}

		});

		lastPosition = position;
		return convertView;

	}


}
