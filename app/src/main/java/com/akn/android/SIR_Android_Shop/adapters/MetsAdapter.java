package com.akn.android.SIR_Android_Shop.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.models.Met;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

public class MetsAdapter extends RecyclerView.Adapter<MetsAdapter.ViewHolder>implements Filterable {

	private List<Met> metList;
	private List<Met> filteredMet;
	private LayoutInflater layoutInflater;
	private Context context;
	private int lastPosition = -1;
   // public static String image_url="";

 	@SuppressWarnings("unchecked")
	public MetsAdapter(Context context, Object o) {

		System.out.println("IIIIIIIIIII==============");
		this.metList = (List<Met>)o;
		this.filteredMet = (List<Met>)o;
		layoutInflater = LayoutInflater.from(context);
 		this.context = context;
	}

	public MetsAdapter(List<Met> metList){

		this.metList = metList;
        this.filteredMet = metList;


    }

	//Create new views (invoked by the layout manager)
	@Override
	public MetsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		//Creating a new view
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,parent,false);

		//set the view's size, margins, paddings and layout parameters

		MetsAdapter.ViewHolder vh = new MetsAdapter.ViewHolder(v);
		return vh;
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		// - get element from arraylist at this position
		// - replace the contents of the view with that element

		holder.tvIdMet.setText(filteredMet.get(position).getIdMet());
		holder.tvNomMet.setText(filteredMet.get(position).getNomMet());
		holder.tvPrixUnitaire.setText(filteredMet.get(position).getPrixUnitaire()+" FCFA");
		holder.tvTps_cuisson.setText(filteredMet.get(position).getTps_cuisson());

        String image_url = filteredMet.get(position).getPic();
        //String picUrl=BASE_URL2+image_url;

        Glide.with(holder.ivPic.getContext())
				.load(image_url)
                .thumbnail(0.70f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivPic);



/*
		int loader = 0;
        String image_url = metList.get(position).getPic();
		String picUrl=URL_IMAGE_SERVER+image_url;
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAA");
        System.out.println(image_url);
		ImageLoader imgLoader = new ImageLoader(context);
		imgLoader.DisplayImage(picUrl, loader, holder.ivPic); */


        //Picasso.with(context).load(image_url).into(holder.ivPic);

		lastPosition = position;
	}

	/*public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return null;
	} */

	//Replace the contents of a view (invoked by the layout manager

	@Override
	public long getItemId(int arg0) {
 		return arg0;
	}

	@Override
	public int getItemCount() {
        //return metList == null ? (0) : metList.size();
        return filteredMet.size();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charstring = constraint.toString();
                if (charstring.isEmpty()) {
                    filteredMet = metList;
                } else {
                    List<Met> filteredList = new ArrayList<>();
                    for (Met row : metList) {
                        if ((row.getNomMet() != null && row.getNomMet().toLowerCase().contains(charstring.toLowerCase().trim()))
                            || (row.getPrixUnitaire() != null && row.getPrixUnitaire().toLowerCase().contains(charstring.toLowerCase().trim()))){

                            filteredList.add(row);
                        }
                    }
                    filteredMet = filteredList;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredMet;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    filteredMet = (ArrayList<Met>) results.values;
                }
                notifyDataSetChanged();
            }
        };
	}
/*
	static class ViewHolder extends RecyclerView.ViewHolder {
 		TextView tvNomMet;
		TextView tvPrixUnitaire;
		TextView tvTps_cuisson;
		TextView tvIdMet;
 		ImageView ivPic;

	}  */


	public static class ViewHolder extends RecyclerView.ViewHolder{
		//each data item is just a string in this case
		public TextView tvIdMet,tvNomMet,tvPrixUnitaire,tvTps_cuisson;
		public  ImageView ivPic;

		public ViewHolder(View v) {
			super(v);
			tvIdMet = (TextView)v.findViewById(R.id.tv_id_met);
			tvNomMet = (TextView) v.findViewById(R.id.tv_nom_met);
			tvPrixUnitaire = (TextView) v.findViewById(R.id.tv_prix_unitaire);
			tvTps_cuisson = (TextView) v.findViewById(R.id.tv_tps_cuisson);
			ivPic = (ImageView) v.findViewById(R.id.iv_pic);
		}
	}

}
