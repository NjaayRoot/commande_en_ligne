package com.akn.android.SIR_Android_Shop.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.RestoListAdapter;
import com.akn.android.SIR_Android_Shop.models.Panier;
import com.akn.android.SIR_Android_Shop.models.Resto;
import com.akn.android.SIR_Android_Shop.rest.ApiClient;
import com.akn.android.SIR_Android_Shop.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;

public class RestaurantActivity extends AppCompatActivity {
    ProgressDialog dialog;
    private ListView listView;
    private ApiInterface apiService;
    public static List<Resto> restoRetrofit = new ArrayList<>();
    private Realm realmResto;
    public static boolean openStatut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listRubrique);

        dialog=new ProgressDialog(this);
        dialog.setMessage(getString(R.string.waiting));
/*
        realmResto.init(this);

        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("resto.realm")
                .schemaVersion(1)
                .migration(new Migration())
                .build();

        Realm.setDefaultConfiguration(realmConfig);


        realmResto = Realm.getDefaultInstance(); */


        //Integrate Retrofit

        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<List<Resto>> callDistanceTime = apiService.getDistanceTime("14.746916","-17.463363");
        callDistanceTime.enqueue(new Callback<List<Resto>>() {
            @Override
            public void onResponse(Call<List<Resto>> call, Response<List<Resto>> response) {
                Log.d("restooo",response.body()+"");
               // System.out.println("ggggggggggg");
                restoRetrofit= response.body();

                Log.d("URL2 Called", call.request().url() + "");
                listView.setAdapter(new RestoListAdapter(getApplicationContext(), restoRetrofit));


            }

            @Override
            public void onFailure(Call<List<Resto>> call, Throwable t) {
               Log.d("Error2",t.getMessage());

            }
        });

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                   // final RealmResults<Resto> restoRealm = realmResto.where(Resto.class).findAll();

                    if (paniers.getListeAchats().size()>=1){
                        paniers = new Panier();
                    }


                    if (restoRetrofit.get(i).getOpenStatut().equalsIgnoreCase("ouvert")){
                        openStatut=true;

                        Intent intent = new Intent(RestaurantActivity.this, MainActivity.class);
                        intent.putExtra("RESTO", restoRetrofit.get(i).getNomResto());
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                    }
                    else {
                        openStatut=false;

                        final AlertDialog.Builder builder2 = new AlertDialog.Builder(RestaurantActivity.this);

                        builder2.setMessage("Désolé "+restoRetrofit.get(i).getNomResto()+" est fermé.\n\nOuvert : "+restoRetrofit.get(i).getHoraireResto()
                                +"\n\nNéanmoins vous pouvez consulter le menu mais vous ne pouvez ni commander ni réserver.\n\nMerci et A Bientot !")
                                .setTitle("Resto Fermé")
                                .setPositiveButton("Consulter le menu", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        Intent intent = new Intent(RestaurantActivity.this, MenuActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                                    }
                                }).setNegativeButton("Choisir autre Resto", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        AlertDialog alertDialog2 = builder2.create();
                        alertDialog2.show();


                    }

                }
            });

    }
}
