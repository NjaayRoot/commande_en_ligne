package com.akn.android.SIR_Android_Shop.models;

/**
 * Created by macbook on 22/10/2017.
 */

public class Accueil {

    private String name;
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
