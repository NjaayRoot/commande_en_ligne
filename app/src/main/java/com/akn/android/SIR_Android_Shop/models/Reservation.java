package com.akn.android.SIR_Android_Shop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macbook on 02/01/2018.
 */

public class Reservation {
    @SerializedName("dateReservation")
    String dateReservation;
    @SerializedName("heureReserv")
    String heureReserv;
}
