package com.akn.android.SIR_Android_Shop.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.MenuListAdapter;
import com.akn.android.SIR_Android_Shop.models.Menu;

import java.util.ArrayList;
import java.util.List;

import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;

public class MenuActivity extends AppCompatActivity {
    private GridView listView;
        public static List<Menu> listMenu=new ArrayList<>();
    private MenuListAdapter menuListAdapter;
    public static TextView cart_badge;
    public ImageView cart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //View viewToolbar = getSupportActionBar().getCustomView();


        //final View headerLayout = toolbar.getRootView();
       // tvEmail = (TextView) headerLayout.findViewById(R.id.tvEmail);
        //tvEmail.setText(user.email);

        listView = (GridView) findViewById(R.id.listMenu);

        cart = (ImageView) findViewById(R.id.cart);
        cart_badge =(TextView) findViewById(R.id.cart_badge);


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this,PanierActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });

        cart_badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this,PanierActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });

        cart_badge.setText(paniers.getListeAchats().size()+"");

       //creating the menu list ( entree or dessert or boissons... )
        listMenu=new ArrayList<>();

        Menu menu=new Menu();
        menu.setImage("entree");
        listMenu.add(menu);

        menu=new Menu();
        menu.setImage("plat");
        listMenu.add(menu);

        menu=new Menu();
        menu.setImage("dessert");
        listMenu.add(menu);

        menu=new Menu();
        menu.setImage("sandwiche");
        listMenu.add(menu);

        menu=new Menu();
        menu.setImage("boisson");
        listMenu.add(menu);

        menu=new Menu();
        menu.setImage("petitdej");
        listMenu.add(menu);


        //initializing adapter

        menuListAdapter = new MenuListAdapter(getApplicationContext(), listMenu);

        //specifying an adapter to access data, create views and replace the content

        listView.setAdapter(menuListAdapter);
        menuListAdapter.notifyDataSetChanged();

        //if we clic on one of the menu it initializ the title of the menu ( entree or dessert or boissons...) and redirect us in MetsActivity

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(MenuActivity.this, MetsActivity.class);
                intent.putExtra("MET", listMenu.get(i).getImage());
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });
    }
}
