package com.akn.android.SIR_Android_Shop.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.models.Client;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    private Toolbar toolbar;
    private Button btnCommande,btnReservation;
    private TextView textView, user_name;
    private ProgressDialog dialog;
    public static String response;
    private ImageView imageFond, logo;
    public  int from=0;
    public String user;
    public static int idClient;
    private Realm realm;
    public static Client clientLog;
    private String restos="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dialog=new ProgressDialog(this);
        dialog.setMessage(getString(R.string.waiting));

        btnCommande = (Button)findViewById(R.id.commander);
        textView = (TextView)findViewById(R.id.textView);
        user_name = (TextView)findViewById(R.id.user_name);

        //it get the title of menu initialized before in MenuActivity
        restos=getIntent().getStringExtra("RESTO");

        textView.setText( "Bienvenue dans le Restaurant du Baol !");


        //imageFond.setImageResource(R.drawable.fond_accueil);
        //logo.setImageResource(R.drawable.logo_couleur);

        btnCommande.setOnClickListener(this);

        realm.init(this);
        realm = Realm.getDefaultInstance();

        clientLog = realm.where(Client.class).findFirst();

      //  System.out.println("ffffffffffff"+clientLog.getIdClient());

        if (clientLog !=null) {
            user_name.setText(clientLog.getName());

            user_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                    builder.setMessage("Nom: " + clientLog.getName() + "\nE-mail: " + clientLog.getEmail())
                            .setTitle("PROFIL RESTOP")
                            .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setPositiveButton("Se déconnecter", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                            builder2.setTitle("Confirmation de la déconnexion")
                                    .setMessage("Voulez vous vraiment nous quitter " + clientLog.getName() + " ?")
                                    .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    }).setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    realm.beginTransaction();
                                    realm.where(Client.class)
                                            .findAll()
                                            .deleteAllFromRealm();
                                    realm.commitTransaction();

                                    user_name.setText("");
                                }
                            });
                            AlertDialog alertDialog2 = builder2.create();
                            alertDialog2.show();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });

        }
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {

        getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.contact_menu:
                startActivity(new Intent(MainActivity.this, ContactActivity.class));

                return true;

            case R.id.help_menu:

                final AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);

                builder2.setMessage("Pour effectuer une commande il faut passer par ces 3 étapes:\n \n" +
                        "1ère étape : Choisir un menu; \n \n" +
                        "2ème étape : Aprés le choix de menu une liste de plat est à votre disposition; " +
                        "vous sélectionnez le plat de votre choix et vous allez remplir la quantité, le mode de consommation " +
                        "et les accompagnements puis vous validez;\n \n" +
                        "3ème étape : Les plats choisis seront répertoriés dans un panier que vous pouvez modifier à volonté " +
                        "et enfin vous validez votre commande.")
                        .setTitle("Commande")
                        .setNegativeButton("Retour", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                AlertDialog alertDialog = builder2.create();
                                alertDialog.show();

                            }
                        }).setPositiveButton("Terminer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog2 = builder2.create();
                alertDialog2.show();



            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.commander:
                startActivity(new Intent(MainActivity.this, MenuActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                break;


        }
    }

    }
