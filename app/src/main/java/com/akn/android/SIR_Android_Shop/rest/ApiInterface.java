package com.akn.android.SIR_Android_Shop.rest;


import com.akn.android.SIR_Android_Shop.models.Met;
import com.akn.android.SIR_Android_Shop.models.Resto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {
   // @SerializedName("mets")
    @GET("type_menu.php")
    //Call<StudentsResponse> getStudentData();
     Call<List<Met>> getMet(@Query("type") String type );

 @GET("mets.php")
 Call<List<Met>> getMets();

    @GET("restaurants.php")
    Call<List<Resto>> getResto();

    @GET("Distance2coords.php")
    Call<List<Resto>> getDistanceTime(@Query("latPos") String latPos,@Query("longPos") String longPos );

/*
    @GET("commande.php")
    Call<String> saveCommande(@Query("totalCmd") int totalCmd, @Query("idUser") int idUser, @Query("codeCmd") String codeCmd);
   */
    @GET("commande.php")
    Call<String> saveCommande(@Query("totalCmd") int totalCmd, @Query("codeCmd") String codeCmd);

    @GET("achat.php")
    Call<String> saveAchats(@Query("quantite") int quantite, @Query("totalAchat") int totalAchat,
                           @Query("condiment") String condiment, @Query("consommation") String consommation,
                           @Query("idMet") int idMet);

 @GET("insertion_reserv.php")
 Call<String> saveReservation(@Query("dateReservation") String dateReservation, @Query("idUser") int idUser,
                                   @Query("heureReserv") String heureReserv, @Query("codeReserv") String codeReserv);

 @GET("insertion_table.php")
 Call<String> savePlace(@Query("typePlace") String typePlace );


}
