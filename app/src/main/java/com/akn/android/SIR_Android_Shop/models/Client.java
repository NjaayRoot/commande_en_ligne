package com.akn.android.SIR_Android_Shop.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by macbook on 10/10/2017.
 */

public class Client extends RealmObject {
    @PrimaryKey
    private int idClient;
    private String name;
    private String email;
    private String gender;
    private String age;
   // private Commande commande;

    public Client(int idClient, String name, String email, String gender, String age) {

        this.idClient = idClient;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.age = age;
        //this.commande = commande;
    }
    public Client(){

    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
/*
    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    */

}