package com.akn.android.SIR_Android_Shop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.PanierAdapter;

import static com.akn.android.SIR_Android_Shop.fragments.MetFragment.panier;

/**
 * A simple {@link Fragment} subclass.
 */
public class PanierFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private PanierAdapter panierAdapter;


    public PanierFragment() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRecyclerView = (RecyclerView) inflater.inflate(
                R.layout.fragment_panier, container, false);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        //initializing adapter
        panierAdapter = new PanierAdapter(panier);

        //specifying an adapter to access data, create views and replace the content
        mRecyclerView.setAdapter(panierAdapter);
        panierAdapter.notifyDataSetChanged();


        return mRecyclerView;
    }


}
