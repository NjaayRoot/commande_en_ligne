package com.akn.android.SIR_Android_Shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.CmdEndListAdapter;
import com.akn.android.SIR_Android_Shop.models.Commande;
import com.akn.android.SIR_Android_Shop.models.Panier;

import java.util.Timer;
import java.util.TimerTask;

import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;
import static com.akn.android.SIR_Android_Shop.activity.PanierActivity.codeCommande;

public class CmdEndActivity extends AppCompatActivity {

   private ListView listView;
    public static TextView  txtValeurTotal,commandeInfo;
    private  Button terminerR4;
    private int idClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cmd_end);
        Toolbar   toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        setSupportActionBar(toolbar);

        terminerR4 = (Button) findViewById(R.id.confirmerPanier);
        commandeInfo = (TextView) findViewById(R.id.commandeInfo);
        txtValeurTotal = (TextView) findViewById(R.id.valeurTotal);

        //Bundle bundle = getIntent().getExtras();

        //String user = bundle.getString("username");
         //idClient = bundle.getInt("useridclient");

        Commande commande= new Commande();

        //commandeInfo.setText(user+", "+commande.getDateCmd()+"\n\n                 Code de commande :     "+codeCommande +"");
        commandeInfo.setText(commande.getDateCmd()+"\n\n   Code de commande :     "+codeCommande +"");




        terminerR4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paniers = new Panier();
                startActivity(new Intent(CmdEndActivity.this, MainActivity.class));
                // overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });



        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listRubrique);

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(CmdEndActivity.this);

// 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("Votre Commande a été enregistrée avec succés\n" +
                "Merci de votre fidélité !")
                .setTitle("Commande");
// 3. Get the AlertDialog from create()
        final AlertDialog dialog = builder.create();
        dialog.show();

        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
            }
        }, 5000); // the timer will count 5 seconds....

        listView.setAdapter(new CmdEndListAdapter(getApplicationContext(), paniers.getListeAchats()));

    }

    }

