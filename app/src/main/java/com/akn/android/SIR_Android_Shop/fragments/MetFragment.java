package com.akn.android.SIR_Android_Shop.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.activity.MainActivity;
import com.akn.android.SIR_Android_Shop.activity.RecyclerItemClickListener;
import com.akn.android.SIR_Android_Shop.activity.SimpleTabsActivity;
import com.akn.android.SIR_Android_Shop.adapters.MetAdapter;
import com.akn.android.SIR_Android_Shop.models.Met;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MetFragment extends Fragment {
    private String []nomMets,prix,tpsCuisson;
    private int [] pics;

    public  void chargerListe(){

        try {
            if(MainActivity.response!=null){

                JSONObject jsonObject=new JSONObject(MainActivity.response);
                JSONArray jsonArray=jsonObject.getJSONArray("mets") ;
                nomMets=new String[jsonArray.length()];
                prix=new String[jsonArray.length()];
                pics=new int[jsonArray.length()];
                //tpsCuisson=new String[jsonArray.length()];

                for (int i = 0; i <jsonArray.length() ; i++) {

                    JSONObject mets=jsonArray.getJSONObject(i);
                    nomMets[i]=mets.getString("nom");
                    prix[i]=mets.getString("prixU");
                    //tpsCuisson[i]=mets.getString("tps_cuisson");
                    //pics[i]=mets.getInt("image");
                    pics[i]= R.drawable.c1;
                }

            }
           // for (int i = 0; i < nomMets.length; i++) {
             //   Met met = new Met(i + 1,nomMets[i], prix[i] , pics[i]);
               // metList.add(met);
            //}
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }



    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    public static List<Met> metList;
    private MetAdapter metAdapter;
    public static ArrayList<Met> panier = new ArrayList<>();

/*
    String[] nomMets = {"Big Burger", "Taste Good", "Frites du Nord", "Crepe salée", "Crepe au Nutella", "Fiiriire",
            "Love Me", "Eate Me", "Cake By The Ocean", "Dangerous Dibi", "Crevette Chinoise", "Pizza Queen", "Crepe with eggs",
            "Middle", "Fire"};

    String[] prix = {"1500 FCFA", "2300 FCFA", "800 FCFA", "2000 FCFA", "2000 FCFA", "2500 FCFA",
            "3200 FCFA", "1500 FCFA", "4000 FCFA", "4000 FCFA", "2000 FCFA",
            "5000 FCFA", "3500 FCFA", "3700 FCFA", "5000 FCFA"};


    int[] pics = {
            R.drawable.f1,
            R.drawable.f2,
            R.drawable.f7,
            R.drawable.c3,
            R.drawable.c1,
            R.drawable.g4,
            R.drawable.g6,
            R.drawable.f3,
            R.drawable.c2,
            R.drawable.g1,
            R.drawable.g5,
            R.drawable.f5,
            R.drawable.c5,
            R.drawable.f4,
            R.drawable.g2,
            R.drawable.g3,
            R.drawable.c4,
            };
*/


    final CharSequence[] mode_cons={"Sur place","A emporter"};

    public MetFragment() {

        System.out.println("QQQQQQQQQQQQQQ             QQQQQQQQQQQQQQ");
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        System.out.println("dddddddddddd             bbbbbbbbbbbbbbbb");

        // Inflate the layout for this fragment
        mRecyclerView = (RecyclerView) inflater.inflate(
                R.layout.fragment_met, container, false);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(),2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        metList = new ArrayList<>();




        //adding data from arrays to songlist
       /* for (int i = 0; i < nomMets.length; i++) {
            Met met = new Met(i + 1,nomMets[i], prix[i] , pics[i]);
            metList.add(met);

        }*/


        //initializing adapter
        metAdapter = new MetAdapter(metList);

        //specifying an adapter to access data, create views and replace the content
        mRecyclerView.setAdapter(metAdapter);
        metAdapter.notifyDataSetChanged();

        chargerListe();

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View mview = (View) inflater.inflate(R.layout.dialog_details_cmd, null);

                TextView tvQuantite = (TextView) mview.findViewById(R.id.quantite);
                TextView tvCons = (TextView) mview.findViewById(R.id.consomation);
                RadioGroup rg_cons = (RadioGroup) mview.findViewById(R.id.modecons);
                Button btnValider = (Button) mview.findViewById(R.id.validerdetail);
                Button btnAnnuler = (Button) mview.findViewById(R.id.annulerdetail);
                EditText quantite = (EditText) mview.findViewById(R.id.nombre);


                rg_cons.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                        switch (checkedId){
                            case R.id.emporter: break;
                            case R.id.surplace: break;
                        }
                    }
                });

                mBuilder.setView(mview);

               final  AlertDialog mDialog = mBuilder.create();
                mDialog.show();

                btnValider.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getActivity(),"Ajouté dans le panier",Toast.LENGTH_LONG).show();
                        mDialog.dismiss();
                        Met met = metList.get(position);

                        panier.add(met);

                        getActivity().finish();
                        startActivity(new Intent(getActivity(), SimpleTabsActivity.class));

                        ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(2);
                    }
                });
                btnAnnuler.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        Toast.makeText(getActivity(),"Commande annulé",Toast.LENGTH_LONG).show();
                    }
                });


            }
        }));

        return mRecyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKK");
    }
}


