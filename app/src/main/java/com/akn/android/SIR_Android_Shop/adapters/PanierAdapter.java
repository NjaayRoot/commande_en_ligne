package com.akn.android.SIR_Android_Shop.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.models.Met;

import java.util.List;

/**
 * Created by macbook on 08/10/2017.
 */

public class PanierAdapter extends RecyclerView.Adapter<PanierAdapter.ViewHolder> {
    private List<Met> metList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false);

        //set the view's size, margins, paddings and layout parameters

        PanierAdapter.ViewHolder vh = new PanierAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Met met = metList.get(position);
        //holder.tvIdMet.setText(String.valueOf(met.getIdMet()));
        holder.tvNomMet.setText(met.getNomMet());
       // holder.tvPrixUnitaire.setText("Prix : "+met.getPrixUnitaire()+" FCFA");
       // holder.tvTps_cuisson.setText(met.getTps_cuisson());
    }

    @Override
    public int getItemCount() {
        return metList.size();
    }


    //Provide a reference to the views for each data item
    //Complex data items may need more than one view per item, and
    //you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder{
        //each data item is just a string in this case
        public TextView tvIdMet, tvNomMet, tvPrixUnitaire, tvQuantite, tvCons, tvCondiment;
        public final ImageView ivPic;

        public ViewHolder(View v) {
            super(v);
           // tvIdMet = (TextView)v.findViewById(R.id.tv_id_met);
            tvNomMet = (TextView) v.findViewById(R.id.list_title);
            tvPrixUnitaire = (TextView) v.findViewById(R.id.list_prix);
            tvQuantite = (TextView) v.findViewById(R.id.list_quantite);
            tvCons = (TextView) v.findViewById(R.id.list_cons);
            tvCondiment = (TextView) v.findViewById(R.id.list_condiments);
            //tvTps_cuisson = (TextView) v.findViewById(R.id.tv_tps_cuisson);
            ivPic = (ImageView) v.findViewById(R.id.list_avatar);
        }
    }

    //Provide a suitable constructor
    public PanierAdapter(List<Met> metList){
    this.metList = metList;
    }

}

