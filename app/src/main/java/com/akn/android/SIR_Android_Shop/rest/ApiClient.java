package com.akn.android.SIR_Android_Shop.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

   // public static final String BASE_URL = "http://192.168.21.2/android/AdminBSBMaterialDesign-master/pages/ui/";
    //public static final String BASE_URL = "http://192.168.1.13/api-restop/";
    public static final String BASE_URL = " http://mycompanystart.000webhostapp.com/api-restop/";
    public static final String BASE_URL2 = " http://mycompanystart.000webhostapp.com/api-restop/mets/";




    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }


}
