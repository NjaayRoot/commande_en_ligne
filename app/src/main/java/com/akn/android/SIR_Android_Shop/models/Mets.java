package com.akn.android.SIR_Android_Shop.models;

/**
 * Created by AKN on 25/07/2017.
 */

public class Mets {
    private int idMet, pic;
    private String nomMet, tps_cuisson, prixUnitaire;
    private String url;

    public Mets(int idMet, String nomMet, String prixUnitaire, int pic, String url) {
        this.idMet = idMet;
        this.pic = pic;
        this.prixUnitaire = prixUnitaire;
        this.nomMet = nomMet;
        this.tps_cuisson = tps_cuisson;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdMet() {
        return idMet;
    }

    public void setIdMet(int idMet) {
        this.idMet = idMet;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public String getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(String prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public String getNomMet() {
        return nomMet;
    }

    public void setNomMet(String nomMet) {
        this.nomMet = nomMet;
    }

    public String getTps_cuisson() {
        return tps_cuisson;
    }

    public void setTps_cuisson(String tps_cuisson) {
        this.tps_cuisson = tps_cuisson;
    }
}
