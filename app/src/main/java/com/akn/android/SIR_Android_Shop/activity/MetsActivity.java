package com.akn.android.SIR_Android_Shop.activity;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.MetsAdapter;
import com.akn.android.SIR_Android_Shop.models.Achat;
import com.akn.android.SIR_Android_Shop.models.Met;
import com.akn.android.SIR_Android_Shop.models.Panier;
import com.akn.android.SIR_Android_Shop.rest.ApiClient;
import com.akn.android.SIR_Android_Shop.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MetsActivity extends AppCompatActivity {

    public static List<Met> metList;
    public static List<Achat> listeAchats= new ArrayList<>();
    public static ArrayList<Met> panier = new ArrayList<>();
    public static Panier paniers= new Panier();
    ProgressDialog dialog;
    public String response;
    public static String met="";
    //private ListView listView;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MetsAdapter metAdapter;
    Button btnDisplay;
    public static TextView cart_badge;
    private ApiInterface apiService;
    public static List<Met> metsRetrofit = new ArrayList<>();
    SearchView searchView;
    SearchView.OnQueryTextListener queryTextListener;
    TextView toolbar_title;



    SwipeRefreshLayout swipeRefresh_projet_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_met);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        ImageView cart = (ImageView) findViewById(R.id.cart);
        cart_badge =(TextView) findViewById(R.id.cart_badge);

        toolbar_title = (TextView)findViewById(R.id.toolbar_title);




        swipeRefresh_projet_list = (SwipeRefreshLayout)findViewById(R.id.swipeRefresh_projet_list);


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MetsActivity.this,PanierActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });

        cart_badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MetsActivity.this,PanierActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });

        //the icon of cart is updating if we add the new met
        cart_badge.setText(paniers.getListeAchats().size()+"");


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(MetsActivity.this,2,GridLayoutManager.VERTICAL,false);
        //mLayoutManager = new LinearLayoutManager(MetsActivity.this, LinearLayoutManager.VERTICAL,false);

        mRecyclerView.setLayoutManager(mLayoutManager);



        dialog=new ProgressDialog(this);
        dialog.setMessage(getString(R.string.waiting));

        //it get the title of menu initialized before in MenuActivity
        met=getIntent().getStringExtra("MET");

        //Integrate Retrofit
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Met>> call= apiService.getMet(met);
         /*Log the URL called*/
        Log.d("URL Called", call.request().url() + "");

        call.enqueue(new Callback<List<Met>>() {
            @Override
            public void onResponse(Call<List<Met>> call, Response<List<Met>> response) {
               metsRetrofit= response.body();

                Log.d("response cmd", response.body()+"");
                swipeRefresh_projet_list.setRefreshing(false);


                showMet();
            }

            @Override
            public void onFailure(Call<List<Met>> call, Throwable t) {
                Log.e("Error",t.getMessage());
            }
        });

        swipeRefresh_projet_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Call<List<Met>> call= apiService.getMet(met);
                /*Log the URL called*/
                Log.d("URL Called", call.request().url() + "");

                call.enqueue(new Callback<List<Met>>() {
                    @Override
                    public void onResponse(Call<List<Met>> call, Response<List<Met>> response) {
                        metsRetrofit= response.body();

                        Log.d("response cmd", response.body()+"");
                        swipeRefresh_projet_list.setRefreshing(false);


                        showMet();
                    }

                    @Override
                    public void onFailure(Call<List<Met>> call, Throwable t) {
                        Log.e("Error",t.getMessage());
                    }
                });
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchManager searchManager = (SearchManager) MetsActivity.this.getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();



        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    toolbar_title.setVisibility(View.GONE);

            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                toolbar_title.setVisibility(View.VISIBLE);
                return false;
            }
        });
        searchView.setSearchableInfo(searchManager.getSearchableInfo(MetsActivity.this.getComponentName()));
        queryTextListener = new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextSubmit", query);
                metAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextChange", newText);

                metAdapter.getFilter().filter(newText);
                return false;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if(item.getItemId()!=android.R.id.home){
            return super.onOptionsItemSelected(item);
        }
        if(item.getItemId()==R.id.action_search){
            return true;
        }
        onBackPressed();
        return true;
        }

       // return super.onOptionsItemSelected(item);


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // this method manage the view of Mets
    public void showMet(){

        metAdapter= new MetsAdapter(metsRetrofit);
        mRecyclerView.setAdapter(metAdapter);

        // the alert dialog where we choose the qte, modecons or condiments
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(MetsActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(MetsActivity.this);
                LayoutInflater inflater = MetsActivity.this.getLayoutInflater();
                View mview = (View) inflater.inflate(R.layout.dialog_details_cmd, null);

                final TextView tvQuantite = (TextView) mview.findViewById(R.id.quantite);
                final TextView condiments = (TextView) mview.findViewById(R.id.condiments);
                TextView tvCons = (TextView) mview.findViewById(R.id.consomation);
                final RadioGroup rg_cons = (RadioGroup) mview.findViewById(R.id.modecons);
                final RadioButton emporter = (RadioButton) mview.findViewById(R.id.emporter);
                final RadioButton surplace = (RadioButton) mview.findViewById(R.id.surplace);
                final Button btnValider = (Button) mview.findViewById(R.id.validerdetail);
                final Button btnAnnuler = (Button) mview.findViewById(R.id.annulerdetail);
                final EditText quantite = (EditText) mview.findViewById(R.id.nombre);
                final CheckBox condiment1 = (CheckBox)mview.findViewById(R.id.condiment1);
                final CheckBox condiment2 = (CheckBox)mview.findViewById(R.id.condiment2);
                final CheckBox condiment3 = (CheckBox)mview.findViewById(R.id.condiment3);
                final CheckBox condiment4 = (CheckBox)mview.findViewById(R.id.condiment4);
                final ImageView Img_delete = (ImageView)mview.findViewById(R.id.Img_delete);

                if (met.equalsIgnoreCase("dessert")){
                    condiments.setText("Parfum :");

                    condiment1.setText("Chocolat");
                    condiment2.setText("Fraise");
                    condiment3.setText("Vanille");
                    condiment4.setText("Café");
                }
                else if (met.equalsIgnoreCase("boisson")){
                    condiments.setText("Version :");

                    condiment1.setText("Petit Modele");
                    condiment2.setText("Grand modele");
                    condiment3.setText("Sans Sucre");
                    condiment4.setText("Gazeuse");
                }
                else if (met.equalsIgnoreCase("petitdej")){
                    condiments.setText("Version :");

                    condiment1.setText("Sans Sucre");
                    condiment2.setText("+ jus");
                    condiment3.setText("Autre");
                    condiment4.setText("Autre");
                }

                mBuilder.setTitle(metsRetrofit.get(position).getNomMet().toUpperCase());
                mBuilder.setView(mview);

                final  AlertDialog mDialog = mBuilder.create();
                mDialog.show();

                btnValider.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mDialog.dismiss();

                      //  if (openStatut==true){

                            //get the selected met
                            final Met met = metsRetrofit.get(position);
                            int quantites = Integer.parseInt(quantite.getText().toString());

                            //Updating the qte
                            if (quantite.getText().toString().equals("")) {
                                met.setQuantite(1);

                            }
                            else {
                                met.setQuantite(Integer.parseInt(quantite.getText().toString()));

                            }

                            //Updating the modecons
                            if (emporter.isChecked()) {

                                met.setCons("emporter");
                                paniers.ajouterAchat(met);
                                cart_badge.setText(paniers.getListeAchats().size()+"");

                            }
                            else if (surplace.isChecked()) {
                                met.setCons("sur_place");
                                paniers.ajouterAchat(met);
                                cart_badge.setText(paniers.getListeAchats().size()+"");

                            }
                            else
                            {
                                final AlertDialog.Builder builder2 = new AlertDialog.Builder(MetsActivity.this);

                                builder2.setMessage("Veuillez choisir le mode de consommation")
                                        .setTitle("Erreur Choix Mets");

                                final AlertDialog alertDialog2 = builder2.create();
                                alertDialog2.show();

                                final Timer timer2 = new Timer();
                                timer2.schedule(new TimerTask() {
                                    public void run() {
                                        alertDialog2.dismiss();
                                        timer2.cancel(); //this will cancel the timer of the system
                                    }
                                }, 3000); // the timer will count 3 seconds....
                            }


                            //Updating the condiment
                            String r = "";
                            if (condiment1.isChecked()) {
                                r = r + condiment1.getText();
                            }
                            if (condiment2.isChecked()) {
                                r = r + "," + condiment2.getText();
                            }
                            if (condiment3.isChecked()) {
                                r = r + "," + condiment3.getText();
                            }
                            if (condiment4.isChecked()) {
                                r = r + "," + condiment4.getText();
                            }
                            met.setCondiment(r.toString());
                       // }
                       // else {

                       //     Toast.makeText(MetsActivity.this,"C'est Fermé",Toast.LENGTH_LONG).show();

                       // }

                    }
                });

                btnAnnuler.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        // Toast.makeText(MetsActivity.this,"Commande annulé",Toast.LENGTH_LONG).show();
                    }
                });

            }
        }));
    }

}
