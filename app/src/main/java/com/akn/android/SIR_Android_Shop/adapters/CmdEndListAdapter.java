package com.akn.android.SIR_Android_Shop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.activity.CmdEndActivity;
import com.akn.android.SIR_Android_Shop.models.Achat;
import com.akn.android.SIR_Android_Shop.models.Met;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;

public class CmdEndListAdapter extends BaseAdapter {

	private List<Achat> listeAchats= null;
	public static  List<Met> metList = null;
	private LayoutInflater layoutInflater;
	private Context context;
	private int lastPosition = -1;
	public static  long sommeTotal= 0;
    public static Met positions;




	@SuppressWarnings("unchecked")
	public CmdEndListAdapter(Context context, Object o) {
		this.listeAchats = (List<Achat>)o;
		layoutInflater = LayoutInflater.from(context);
 		this.context = context;
	}



	@Override
	public int getCount() {
 		return paniers.getListeAchats().size();
	}

	@Override
	public Achat getItem(int position) {
 		return paniers.getListeAchats().get(position);
	}

	@Override
	public long getItemId(int arg0) {
 		return arg0;
	}

	static class ViewHolder {
		public TextView tvIdMet, tvNomMet, tvPrixUnitaire, tvQuantite, tvCons, tvCondiment, edit_qantite, tpsCuisson;
		public Button btn_minus,btn_plus;
		public  ImageView ivPic, Img_delete ;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final ViewHolder holder;

		if (convertView == null) {
			
			convertView = layoutInflater.inflate(R.layout.cmd_end_list, null);
			holder = new ViewHolder();
			// initializ the views
			holder.tvNomMet = (TextView) convertView.findViewById(R.id.list_title);
			holder.tvPrixUnitaire = (TextView) convertView.findViewById(R.id.list_prix);
			holder.tvQuantite = (TextView) convertView.findViewById(R.id.list_quantite);
			holder.tvCons = (TextView) convertView.findViewById(R.id.list_cons);
			holder.tvCondiment = (TextView) convertView.findViewById(R.id.list_condiments);
			holder.ivPic = (ImageView) convertView.findViewById(R.id.list_avatar);

			CmdEndActivity.txtValeurTotal.setText("00 FCFA");

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}


		holder.tvNomMet.setText(paniers.getListeAchats().get(position).getMet().getNomMet().toUpperCase()+"");
		holder.tvPrixUnitaire.setText("Prix : "+paniers.getListeAchats().get(position).getTotalAchat()+" FCFA");
		holder.tvQuantite.setText("Quantite : "+paniers.getListeAchats().get(position).getMet().getQuantite()+"");
		holder.tvCons.setText("Consommation : "+paniers.getListeAchats().get(position).getMet().getCons()+"");
		holder.tvCondiment.setText("Accompagnements : "+paniers.getListeAchats().get(position).getMet().getCondiment()+"");
/*
		int loader = 0;
		String image_url = paniers.getListeAchats().get(position).getMet().getPic();
		System.out.println(image_url);
		ImageLoader imgLoader = new ImageLoader(context).DisplayImage(Utils.URL_IMAGE_SERVER+image_url, loader, holder.ivPic);
		*/

        String image_url = paniers.getListeAchats().get(position).getMet().getPic();
		//String picUrl=BASE_URL+image_url;

		Glide.with(context).load(image_url)
				.thumbnail(0.0f)
				.crossFade()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.into(holder.ivPic);

              positions = paniers.getListeAchats().get(position).getMet();


		CmdEndActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");

		lastPosition = position;
		return convertView;

	}

}
