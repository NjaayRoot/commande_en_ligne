package com.akn.android.SIR_Android_Shop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.adapters.PanierListAdapter;
import com.akn.android.SIR_Android_Shop.models.Client;
import com.akn.android.SIR_Android_Shop.rest.ApiClient;
import com.akn.android.SIR_Android_Shop.rest.ApiInterface;

import java.nio.ByteBuffer;
import java.util.UUID;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;


import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;

public class PanierActivity extends AppCompatActivity {

    private ListView listView;
    public static TextView edit_qantite, txtValeurTotal, cart_badge;
    Button btn_minus, btn_plus, modifierPanier, confirmerPanier;
    ImageView Img_delete;
    public static int total=0;
    ProgressDialog dialog;
    public String response;
    private Realm realm;
    public static Client clientLog;
    public static String codeCommande="";
    private static ApiInterface apiService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_accueil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarR1);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listRubrique);

        dialog=new ProgressDialog(this);
        dialog.setMessage(getString(R.string.waiting));


        final TextView tvQuantite = (TextView) findViewById(R.id.quantite);
        edit_qantite = (TextView) findViewById(R.id.edit_text);
        btn_minus = (Button) findViewById(R.id.btn_minus);
        btn_plus = (Button) findViewById(R.id.btn_plus);
        Img_delete = (ImageView) findViewById(R.id.Img_delete);
        modifierPanier = (Button) findViewById(R.id.modifierPanier);
        confirmerPanier = (Button) findViewById(R.id.confirmerPanier);
        txtValeurTotal = (TextView) findViewById(R.id.valeurTotal);
        cart_badge =(TextView) findViewById(R.id.cart_badge);

        realm.init(this);

        realm = Realm.getDefaultInstance();

        clientLog = realm.where(Client.class).findFirst();

        modifierPanier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PanierActivity.this, MenuActivity.class));
                //overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        confirmerPanier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(PanierActivity.this, "Commande passée avec succés", Toast.LENGTH_LONG).show();
                if (paniers.getListeAchats().size()==0){
                    Toast.makeText(PanierActivity.this, "Le panier est vide", Toast.LENGTH_LONG).show();

                }
               // else if (clientLog== null){

                   // Intent intent = new Intent(PanierActivity.this, LoginActivity.class);
                    //startActivity(intent);

                //}
               // else if (clientLog!=null){
                    //insertCommande();

                else {
                    insertCommande2();

                    Intent intent = new Intent(PanierActivity.this, CmdEndActivity.class);
                    //intent.putExtra("username", clientLog.getName());
                    //intent.putExtra("useridclient", clientLog.getIdClient());
                    startActivity(intent);

                }



                //}

            }
        });

        listView.setAdapter(new PanierListAdapter(getApplicationContext(), paniers.getListeAchats()));

    }
    public static void insertCommande2(){


        int total= paniers.getTotalPanier();


        // handling the random code
        UUID uuid = UUID.randomUUID();
        int c = ByteBuffer.wrap(uuid.toString().getBytes()).getInt();
        UUID uuid2 = UUID.randomUUID();
        int c2 = ByteBuffer.wrap(uuid2.toString().getBytes()).getInt();

        String codeCommande1 = Integer.toString(c, Character.MAX_RADIX);
        String codeCommande2 = Integer.toString(c2, Character.MAX_RADIX);
        codeCommande =codeCommande1+"-"+codeCommande2;

        //Integrate Retrofit
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call= apiService.saveCommande(total, codeCommande);
                         /*Log the URL called*/
        Log.d("URL Called", call.request().url() + "");

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                //this method insert achats of command
                insertAchat2();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });


    }

    public static void insertAchat2(){

        for (int i=0;i<paniers.getListeAchats().size();i++){

            int qte= paniers.getListeAchats().get(i).getMet().getQuantite();
            int totalAch = paniers.getListeAchats().get(i).getTotalAchat();
            int idPlat =Integer.parseInt(paniers.getListeAchats().get(i).getMet().getIdMet());
            String cons= paniers.getListeAchats().get(i).getMet().getCons();
            String condiments= paniers.getListeAchats().get(i).getMet().getCondiment();

            //Integrate Retrofit
            apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<String> call2= apiService.saveAchats(qte, totalAch, condiments, cons, idPlat);
                         /*Log the URL called*/
            Log.d("URL Called 2", call2.request().url() + "");

            call2.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                }
            });

        }
    }
}

