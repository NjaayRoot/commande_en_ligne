package com.akn.android.SIR_Android_Shop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.models.Achat;
import com.akn.android.SIR_Android_Shop.models.Met;
import com.akn.android.SIR_Android_Shop.models.Resto;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import static com.akn.android.SIR_Android_Shop.activity.MetsActivity.paniers;
import static com.akn.android.SIR_Android_Shop.rest.ApiClient.BASE_URL;

public class RestoListAdapter extends BaseAdapter {

	private List<Resto> listeResto= null;
	//public static  List<Met> metList = null;
	private LayoutInflater layoutInflater;
	private Context context;
	private int lastPosition = -1;
    public static Met positions;


	@SuppressWarnings("unchecked")
	public RestoListAdapter(Context context, Object o) {
		this.listeResto = (List<Resto>)o;
		layoutInflater = LayoutInflater.from(context);
 		this.context = context;
	}


	@Override
	public int getCount() {
 		return listeResto.size();
	}

	@Override
	public Achat getItem(int position) {
 		return paniers.getListeAchats().get(position);
	}

	@Override
	public long getItemId(int arg0) {
 		return arg0;
	}

	static class ViewHolder {

        public TextView tvNomResto, tvLieu, tvhoraire, tvDistance, tvDuree;
		public  ImageView ivPic;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final ViewHolder holder;

		if (convertView == null) {
			
			convertView = layoutInflater.inflate(R.layout.resto_row, null);
			holder = new ViewHolder();
			// initializ the views
			holder.tvNomResto = (TextView) convertView.findViewById(R.id.list_title);
			holder.tvLieu = (TextView) convertView.findViewById(R.id.list_lieu);
			holder.tvhoraire = (TextView) convertView.findViewById(R.id.list_horaire);
			holder.tvDistance = (TextView) convertView.findViewById(R.id.list_distance);
			holder.tvDuree = (TextView) convertView.findViewById(R.id.list_duree);
			holder.ivPic = (ImageView) convertView.findViewById(R.id.list_avatar);


			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		//Set all the views

		holder.tvNomResto.setText(listeResto.get(position).getNomResto());
		holder.tvhoraire.setText("Ouvert : "+listeResto.get(position).getHoraireResto());
		holder.tvLieu.setText("Lieu : "+listeResto.get(position).getLieuResto()+"");
		holder.tvDistance.setText("Distance : "+listeResto.get(position).getDistance());
		holder.tvDuree.setText("Duree : "+listeResto.get(position).getTime());

		//int loader = 0;
		//System.out.println(image_url);
		//ImageLoader imgLoader = new ImageLoader(context).DisplayImage(Utils.URL_IMAGE_SERVER+image_url, loader, holder.ivPic);

		String image_url = listeResto.get(position).getImageResto();

		String picUrl=BASE_URL+image_url;

		Glide.with(context).load(picUrl)
				.thumbnail(0.0f)
				.crossFade()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.into(holder.ivPic);

/*


		positions = paniers.getListeAchats().get(position).getMet();

        //Clic on image delete
		holder.Img_delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                //call the method supprimerAchat in Panier model
                paniers.supprimerAchat(paniers.getListeAchats().get(position).getMet());

                PanierActivity.txtValeurTotal.setText(paniers.getTotalPanier()+" FCFA");
				notifyDataSetChanged();

			}
		});

		*/

		lastPosition = position;
		return convertView;

	}
}
