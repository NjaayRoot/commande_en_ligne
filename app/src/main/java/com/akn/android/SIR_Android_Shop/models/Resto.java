package com.akn.android.SIR_Android_Shop.models;


/**
 * Created by AKN on 25/07/2017.
 */

public class Resto {

    private String nomResto;
    private String lieuResto;
    private String imageResto;
    private String horaireResto;
    private String openStatut;
    private String distance;
    private String time;


    public Resto(String nomResto, String lieuResto, String imageResto, String horaireResto) {
        this.nomResto = nomResto;
        this.lieuResto = lieuResto;
        this.imageResto = imageResto;
        this.horaireResto = horaireResto;
    }

    public Resto(){

    }

    public String getNomResto() {
        return nomResto;
    }

    public void setNomResto(String nomResto) {
        this.nomResto = nomResto;
    }

    public String getLieuResto() {
        return lieuResto;
    }

    public void setLieuResto(String lieuResto) {
        this.lieuResto = lieuResto;
    }

    public String getImageResto() {
        return imageResto;
    }

    public void setImageResto(String imageResto) {
        this.imageResto = imageResto;
    }

    public String getHoraireResto() {
        return horaireResto;
    }

    public void setHoraireResto(String horaireResto) {
        this.horaireResto = horaireResto;
    }

    public String getOpenStatut() {
        return openStatut;
    }

    public void setOpenStatut(String openStatut) {
        this.openStatut = openStatut;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
