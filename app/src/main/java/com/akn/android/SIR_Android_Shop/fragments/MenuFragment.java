package com.akn.android.SIR_Android_Shop.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.akn.android.SIR_Android_Shop.R;
import com.akn.android.SIR_Android_Shop.activity.SimpleTabsActivity;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

    Button btnEntree, btnPlat, btnDessert,btnSandwiche,btnPtit_dej,btnSoda;
    private String []typeMenu,entree;
    private ProgressDialog dialog;
    public static String response;
/*
    public void chargerListe(){

        try {
            if(response!=null){

                JSONObject jsonObject=new JSONObject(response);
                JSONArray jsonArray=jsonObject.getJSONArray("mets") ;
                typeMenu=new String[jsonArray.length()];


                for (int i = 0; i <jsonArray.length() ; i++) {

                    JSONObject mets=jsonArray.getJSONObject(i);
                    typeMenu[i]=mets.getString("type");

                }

            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
*/
    public MenuFragment() {
        // Required empty public constructor
    }

    protected    class MenuServer extends AsyncTask<String, Void, String>

    {

        @Override
        protected void onPreExecute() {
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                HttpClient client=new DefaultHttpClient();
                HttpGet get=new HttpGet(params[0]);
                ResponseHandler<String> buffer=new BasicResponseHandler();
                String result=client.execute(get, buffer);
                return result;
            }catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("",result.toString());
            dialog.dismiss();
            response=result;
            try {

                if(result==null){
                    Toast.makeText(((SimpleTabsActivity)getActivity()), "Error", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(((SimpleTabsActivity)getActivity()), "Successful", Toast.LENGTH_SHORT).show();
                }

            }catch (Exception e)
            {

            }

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        dialog=new ProgressDialog(((SimpleTabsActivity)getActivity()));
        dialog.setMessage(getString(R.string.waiting));


        btnEntree =(Button) view.findViewById(R.id.btnEntree);
        btnPlat =(Button)view.findViewById(R.id.btnPlat);
        btnDessert =(Button)view.findViewById(R.id.btnDessert);
        btnSandwiche =(Button)view.findViewById(R.id.btnSandwiche);
        btnPtit_dej =(Button)view.findViewById(R.id.btnPtit_dej);
        btnSoda =(Button)view.findViewById(R.id.btnSoda);




         btnEntree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              // MetsServer metsServer=new MetsServer();
               //metsServer.execute("http://192.168.21.2/android/entree.php?type=ENTREE");

            ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);


                     }
        });

        btnPlat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);
             }
        });

        btnDessert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);
                Toast.makeText(((SimpleTabsActivity)getActivity()),"Desserts !!!",Toast.LENGTH_LONG).show();
            }
        });

        btnSandwiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);
                Toast.makeText(((SimpleTabsActivity)getActivity()),"Sandwiches à votre diisposition",Toast.LENGTH_LONG).show();
            }
        });

        btnPtit_dej.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);
                Toast.makeText(((SimpleTabsActivity)getActivity()),"Petit déj !!!",Toast.LENGTH_LONG).show();
            }
        });

        btnSoda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);
                Toast.makeText(((SimpleTabsActivity)getActivity()),"Boissons à votre diisposition",Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }

    protected    class MetsServer extends AsyncTask<String, Void, String>

    {

        @Override
        protected void onPreExecute() {
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                HttpClient client=new DefaultHttpClient();
                HttpGet get=new HttpGet(params[0]);
                ResponseHandler<String> buffer=new BasicResponseHandler();
                String result=client.execute(get, buffer);
                return result;
            }catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("",result.toString());
            dialog.dismiss();
            try {

                response=result;

                System.out.println("oooooooooooooooooo oooooooooooooooooo-"+response);


                //Intent intent=new Intent(getActivity(), SimpleTabsActivity.class);
                //startActivity(intent);
                chargerListe(response);

                 ((SimpleTabsActivity)getActivity()).viewPager.setCurrentItem(1);

            }catch (Exception e)
            {

            }

        }
    }

    private String []nomMets,prix,tpsCuisson;
    private int [] pics;

    public  void chargerListe(String response){

        try {
            if(response!=null){

                MetFragment.metList =new ArrayList<>();

                JSONObject jsonObject=new JSONObject(response);
                JSONArray jsonArray=jsonObject.getJSONArray("mets") ;
                nomMets=new String[jsonArray.length()];
                prix=new String[jsonArray.length()];
                pics=new int[jsonArray.length()];
                //tpsCuisson=new String[jsonArray.length()];

                for (int i = 0; i <jsonArray.length() ; i++) {

                    JSONObject mets=jsonArray.getJSONObject(i);
                    nomMets[i]=mets.getString("nom");
                    prix[i]=mets.getString("prixU");
                    //tpsCuisson[i]=mets.getString("tps_cuisson");
                    //pics[i]=mets.getInt("image");
                    pics[i]=R.drawable.c1;

                    System.out.println("-----"+prix[i]);
                }

            }
           // for (int i = 0; i < nomMets.length; i++) {
             //   Met met = new Met(i + 1,nomMets[i], prix[i] , pics[i]);
              //  MetFragment.metList.add(met);
            //}
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
