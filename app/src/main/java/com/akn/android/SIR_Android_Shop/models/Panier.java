package com.akn.android.SIR_Android_Shop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Panier implements Serializable {

    private List<Achat> listeAchats;

    @SerializedName("totalCmd")
    private int totalPanier;

    private String ajoutRef;
    private String supprimRef;


    public Panier() {
		this.listeAchats = new ArrayList();
	    }



    public Achat rechercherAchat(Met m) {
        for (Achat a : listeAchats) {
            if (a.getMet().getIdMet().equals(m.getIdMet())) {
                return a;
            }
        }
        return null;
    }

    public List<Achat> getListeAchats() {
        return listeAchats;
    }

    public void ajouterAchat(Met m) {
        Achat achat = rechercherAchat(m);
        if (achat != null) {
            int quantity = achat.getMet().getQuantite();
            achat.getMet().setQuantite(++quantity);
        } else {
//            int quantity = achat.getQuantite();

            achat = new Achat(m);
            listeAchats.add(achat);
        }

    }

    public void supprimerAchat(Met m) {
        Achat achat = rechercherAchat(m);
        if (achat != null) {
            int quantity = achat.getMet().getQuantite();
            if (quantity > 1) {
                achat.getMet().setQuantite(--quantity);
            }
            else {
                listeAchats.remove(achat);

            }
        }
    }

    public int getTotalPanier() {
        int sommeTotalAchat = 0;
        for (Achat a : listeAchats) {
            sommeTotalAchat += a.getTotalAchat();
        }
        setTotalPanier(sommeTotalAchat);
        return sommeTotalAchat;
    }

    public void setTotalPanier(int totalPanier) {
        this.totalPanier = totalPanier;
    }


}
